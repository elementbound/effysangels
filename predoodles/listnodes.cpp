#include <iostream>
#include <osmium/io/any_input.hpp>
#include <osmium/handler.hpp>
#include <osmium/visitor.hpp>

using namespace std;

class DatHandler : public osmium::handler::Handler
{
	public:
		void node(const osmium::Node& n)
		{
			cout << "[" << n.id() << "]" << n.location() << endl;
		}
};

int main(int argc, char** argv)
{
	if(argc<2)
	{
		cout << "And now what am I supposed to do? Imagine a map and draw some shit on it? " << endl;
		return 0;
	}
	
	//Mit olvasson be? A ZÖSSZESET
	osmium::io::Reader reader(argv[1], osmium::osm_entity_bits::node);
	
	cout << "Print ALL the places!" << endl;
	osmium::memory::Buffer buff;
	DatHandler handler;
	while(buff = reader.read()) //reader.read() is my captain obvious call of the day
		osmium::apply(buff, handler);
	
	return 0;
}
