#include <iostream>
#include <osmium/osm.hpp>
#include <osmium/io/any_input.hpp>

using namespace std;

int main(int argc, char** argv)
{
	if(argc<2)
	{
		cout << "And now what am I supposed to do? Imagine a map and draw some shit on it? " << endl;
		return 0;
	}
	
	//Mit olvasson be? A ZÖSSZESET
	osmium::io::Reader reader(argv[1], osmium::osm_entity_bits::all);
	
	cout << "Print ALL the places!" << endl;
	osmium::memory::Buffer buff;
	while(buff = reader.read())
	{
		for(auto& item: buff)
		{
			if(item.type() != osmium::item_type::node)
				continue;
				
			osmium::Node& n = static_cast<osmium::Node&>(item);
			cout << "[" << n.id() << "]" << n.location() << endl;
		}
	}
	
	return 0;
}
