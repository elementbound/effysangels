#include <iostream>
#include <osmium/osm.hpp>
#include <osmium/io/any_input.hpp>
#include <string>
#include <vector>
#include <map>

using namespace std;

struct bus_route
{
	std::string name;
	std::string from;
	std::string to;
	
	std::vector<osmium::object_id_type> stops;
};

int main(int argc, char** argv)
{
	if(argc<2)
	{
		cout << "And now what am I supposed to do? Imagine a map and draw some shit on it? " << endl;
		return 0;
	}
	
	std::vector<bus_route> routes;
	std::map<osmium::object_id_type, std::string> node_names;
	
	osmium::io::Reader reader(argv[1], osmium::osm_entity_bits::all);
	
	osmium::memory::Buffer buff;
	
	while(buff = reader.read())
	{
		for(auto& item: buff)
		{
			if(item.type() == osmium::item_type::node)
			{
				osmium::Node& n = static_cast<osmium::Node&>(item);
				if(n.tags().get_value_by_key("name"))
					node_names.insert({n.id(), n.tags()["name"]});
					
				continue;
			}
			
			if(item.type() != osmium::item_type::relation)
				continue;
				
			osmium::Relation& r = static_cast<osmium::Relation&>(item);
			
			if(std::string(r.tags().get_value_by_key("route","")) != "bus")
				continue;
				
			bus_route new_route;
			if(r.tags()["ref"])
				new_route.name = r.tags()["ref"];
			else if(r.tags()["name"])
				new_route.name = r.tags()["name"];
			else
				new_route.name = "???";
				
			new_route.from = r.tags()["from"];
			new_route.to = r.tags()["to"];
				
			for(auto& m : r.members())
			{
				if(m.type() != osmium::item_type::node ||
				   strcmp(m.role(), "stop") != 0)
					continue;
				
				new_route.stops.push_back(m.ref());
			}
			
			routes.push_back(new_route);
		}
	}
	
	for(auto& route : routes)
	{
		cout << "[" << route.name << "]" << route.from << " -> " << route.to << endl;
		for(auto& stop : route.stops)
			cout << "    " << node_names[stop] << endl;
			
		cout << endl;
	}
	
	cout << "Found " << routes.size() << " route" << ((routes.size()>1)?"s":"") << endl;
	
	return 0;
}
