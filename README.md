# README #

## Licensz ##
Egyik szerfelett hasznos információ: 
Mindenikféle forráskódra, ahol nincs külön feltűntetve, az alábbi licensz
vonatkozik: http://creativecommons.org/licenses/by-nc-sa/4.0/

Third-party dolgok:
https://code.google.com/p/osm-parser/ - MIT License 
https://github.com/osmcode/libosmium - Boost Software License 
Értelemszerűen a használt third-party forrásokra a saját licenszük vonatkozik,
a fentebbi linkeken megtalálhatóak. 

## Használat ##
Minden feladatnak van saját mappája. A mappában a kész feladathoz mellékelve
jár egy makefile, mely lefordítja és példainputtal futtatja a programot.
Példának okáért: 

	adminus@moebius:~/dev/effysangels$ cd petra
	adminus@moebius:~/dev/effysangels/petra$ make
	mkdir -p ../bin
	g++ petra.cpp -std=c++11 -lexpat -pthread -lz -lprotobuf-lite -losmpbf -lz -lbz2 -pedantic -o ../bin/petra
	../bin/petra ../debrecen.osm 47.2100 21.9909 47.6659 21.6524 > route
	Traversing: 120192/120196
	Backtracking: 26754803 -> 717925269: 717925269 (0)[294]                      
	Route length: 62888.8
	adminus@moebius:~/dev/effysangels/petra$ 

## Feladatok ##
És az illető feladatkiírás mely alapján dolgozunk: 

* MALVIN feladat  (40 pont)
Írj C++ programot, amely kiírja az összes debreceni buszjárat megállóit,
járatonként rendezve, azaz járatonként a megállókat.
    Címkék: City, Map, OSM, libosmium.

* KOPPÁNY feladat (40 pont)
    Írj OSM alapú megjelenítõ programot a Malvin feladathoz!
        Címkék: City, Map, Monitors.

* DÉNES feladat   (35 pont)
    Írj Android Java/Google Maps alapú megjelenítõ programot a Malvin
    feladathoz!
        Címkék: City, Map, Monitors.

* GEDEON feladat  (30 pont)
    A Malvin feladat Java megoldása Java-ban!
        Címkék: City, Map.

* PETRA feladat   (120 pont)
Írj C++ programot, amely bemenetként egy OSM térképet és két a térképen
lévõ GPS koordináta párt kap, majd kimenetként kinyomtat egy utat a
megadott koordináta párokhoz legközelebb esõ két csomópont között.
    Címkék: City, Map, OSM, routing.

* BRÚNÓ feladat   (50 pont)
    Írj OSM alapú megjelenítõ programot a Petra feladathoz!
        Címkék: City, Map, Monitors.

* AMÁLIA feladat  (30 pont)
    Írj Android Java/Google Maps alapú megjelenítõ programot a Petra
    feladathoz!
        Címkék: City, Map, Monitors.

* EDE feladat     (70 pont)
    A Petra feladat Java megoldása R-ben!
        Címkék: City, Map, routing, R.

* BRIGITTA feladat    (90 pont)
    A Petra feladat Java megoldása Java-ban!
        Címkék: City, Map, routing.

* HELGA feladat   (240 pont)
Írj C++ programot, amely a korábbi hangya szimulációs megközelítést
adaptálja OSM-re: a jármûvek a debreceni térképen random bolyongjanak,
de használjanak olyan heurisztikát, miszerint arra mennek, amerre többen
mennek (ez a hangya szimulációs megközelítés). Ha a jármûkibolyongna
Debrecen térképrõl, akkor egy random választott úton lépjen vissza a
városba (olyan megközelítéssel, mint a sejtautomata szimulációkban a
periódikus határfeltételünk volt, de ott nem volt random, ha a jobb
oldalon kilépett a sejttérbõl, akkor a balon lépett vissza stb.).
    Címkék: City, Map, OSM, routing, Robocar City Emulator.

* FERENC feladat  (150 pont)
    Írj OSM alapú megjelenítõ programot a Helga feladathoz!
        Címkék: City, Map, Monitors.

* AURÉL feladat   (90 pont)
    Írj Android Java/Google Maps alapú megjelenítõ programot a Helga
    feladathoz!
        Címkék: City, Map, Monitors.

* MIKSA feladat   (190 pont)
    A Helga feladat Java megoldása Java-ban!
        Címkék: City, Map, routing, Robocar City Emulator.

* HELÉN feladat   (320 pont)
Írj olyan C++ programot, amely egy adott út forgalmát mutató
videófelvételrõl kiszámolja az adott irányú forgalmat egy olyan függvény
formájában, mely megadja, hogy az addig eltelt percekben mennyi
jármû haladt át.
    Címkék: City, Map, videó és képfeldolgozás.

* TERÉZ feladat   (100 pont)
Írj forgalomirányító algoritmust a Debreceni Robotautó Világbajnokságra!
(Csak a robocar-emulator, Robocar World Championship - Robocar City
Emulator élesítése után választható.)
    Címkék: Robocar City Emulator.
