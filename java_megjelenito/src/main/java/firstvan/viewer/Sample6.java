package firstvan.viewer;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;

import javax.swing.JFrame;
import javax.swing.JToolTip;

import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactoryInfo;
import java.io.File;
import java.io.FileNotFoundException;
import static java.time.Clock.system;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;
import org.jxmapviewer.painter.CompoundPainter;
import org.jxmapviewer.painter.Painter;
import java.util.Set;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.DefaultWaypoint;

/**
 * A simple sample application that uses JXMapKit
 *
 * @author Martin Steiger
 */
public class Sample6 {

    /**
     * @param args the program args (ignored)
     */
    public static void main(String[] args) {
        final JXMapKit jXMapKit = new JXMapKit();
        TileFactoryInfo info = new OSMTileFactoryInfo();
        DefaultTileFactory tileFactory = new DefaultTileFactory(info);
        jXMapKit.setTileFactory(tileFactory);
        List<GeoPosition> megallok = new ArrayList<>();
        Scanner sc = null;

        final JToolTip tooltip = new JToolTip();
        tooltip.setTipText("Java");
        tooltip.setComponent(jXMapKit.getMainMap());
        jXMapKit.getMainMap().add(tooltip);
        final GeoPosition gp = new GeoPosition(47.540968, 21.640548);
        jXMapKit.setZoom(7);
        jXMapKit.setAddressLocation(gp);

        List<GeoPosition> waypoints;
        waypoints = new ArrayList<>();

        WaypointPainter<Waypoint> waypointPainter = new WaypointPainter<>();

        jXMapKit.getMainMap().addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                // ignore
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                JXMapViewer map = jXMapKit.getMainMap();

                // convert to world bitmap
                Point2D worldPos = map.getTileFactory().geoToPixel(gp, map.getZoom());

                // convert to screen
                Rectangle rect = map.getViewportBounds();
                int sx = (int) worldPos.getX() - rect.x;
                int sy = (int) worldPos.getY() - rect.y;
                Point screenPos = new Point(sx, sy);

                // check if near the mouse
                if (screenPos.distance(e.getPoint()) < 20) {
                    screenPos.x -= tooltip.getWidth() / 2;

                    tooltip.setLocation(screenPos);
                    tooltip.setVisible(true);
                } else {
                    tooltip.setVisible(false);
                }
            }
        });

        // Display the viewer in a JFrame
        JFrame frame = new JFrame("JXMapviewer2 Example 6");
        frame.getContentPane().add(jXMapKit);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        if (args.length == 0)
        {
         JOptionPane.showMessageDialog(frame, "NEM ADOTT MEG INPUTE FILE-T");
        }
        else{
        try {
            sc = new Scanner(new File(args[0]));
        
        while (sc.hasNext()) {
            double lat = sc.nextDouble();
            double lon = sc.nextDouble();
            megallok.add(new GeoPosition(lat, lon));
        }
        RoutePainter rp = new RoutePainter(megallok);

		//waypointPainter.setWaypoints(waypoints);
        List<Painter<JXMapViewer>> painters;
        painters = new ArrayList<>();
        //painters.add(waypointPainter);
        painters.add(rp);
        CompoundPainter<JXMapViewer> painter = new CompoundPainter<>(painters);
        jXMapKit.getMainMap().setOverlayPainter(painter);
        
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(frame, "NEM LETEZIK INPUT FILE");
        }
        }
            
    }

}
