#include <osmium/osm.hpp>
#include <osmium/io/any_input.hpp>
#include <cstdlib> //atoll
#include <vector>
#include <string>
#include <sstream>
#include "graph.hpp"

osmium::Location loc_from_string(const char* latstr, const char* lonstr)
{
	osmium::Location ret;
	static std::stringstream locparse;
	double rd;
	
	//X
	locparse.str(latstr);
	locparse.seekg(std::ios_base::beg);
	locparse >> rd;
	ret.set_lat(rd);
	
	//Y
	locparse.str(lonstr);
	locparse.seekg(std::ios_base::beg);
	locparse >> rd;
	ret.set_lon(rd);
	
	return ret;
}

typedef std::vector<osmium::object_id_type> way_data;

//============================================================================================//

bool cmd_Quiet = 0;
bool cmd_UseDijkstra = 1;
bool cmd_GotLocStart = 0;
bool cmd_GotLocEnd = 0;
bool cmd_GotMap = 0;

osmium::object_id_type cmd_StartId = 0;
osmium::object_id_type cmd_EndId = 0;

std::string cmd_MapPath;
osmium::Location cmd_Start;
std::vector<osmium::Location> cmd_End;

int main(int argc, char** argv)
{
	//========================================================================================//
	//Parse command line args
	
	for(unsigned i=1; i<argc; i++)
	{
		static std::string arg;
		arg = argv[i];
		
		if(arg == "--map")
		{
			if(argc-i < 1)
				continue; 
			
			cmd_MapPath = argv[++i];
			cmd_GotMap = 1;
		} else
		if(arg == "--start")
		{
			if(argc-i < 2)
				continue; 
				
			cmd_Start.set_lat(atof(argv[++i]));
			cmd_Start.set_lon(atof(argv[++i]));
			cmd_GotLocStart = 1;
		} else 
		if( arg == "--start-id")
		{
			if(argc-i < 1)
				continue;
				
			cmd_StartId = atoll(argv[++i]);
			cmd_GotLocStart=1;
		} else
		if(arg == "--end")
		{
			if(argc-i < 2)
				continue; 
			
			osmium::Location nl;
			nl.set_lat(atof(argv[++i]));
			nl.set_lon(atof(argv[++i]));
			cmd_End.push_back(nl);
			
			cmd_GotLocEnd = 1;
		} else 
		if( arg == "--end-id" )
		{
			if(argc-i < 1)
				continue;
				
			cmd_EndId = atoll(argv[++i]);
			cmd_GotLocEnd = 1;
		} else
		if(arg == "--use-dijkstra")
			cmd_UseDijkstra = 1;
		else if(arg == "--no-dijkstra")
			cmd_UseDijkstra = 0;
		else if(arg == "--quiet" || arg == "-q")
			cmd_Quiet=1;
		else 
			std::cerr << "Unknown flag: " << arg << std::endl;
	}
	
	if(cmd_Quiet)
		std::cerr.setstate(std::ios::badbit);
	
	if(!cmd_GotMap)
	{
		std::cerr << "And now what am I supposed to do? Imagine a map and draw some shit on it? " << std::endl;
		return 1;
	}
	
	if(!cmd_GotLocEnd || !cmd_GotLocStart)
	{
		std::cerr << "Get me some coordinates for fuck's sake I'm not pathfindig to Neverland" << std::endl;
		return 2;
	}
	
	//========================================================================================//
	
	//Feelin like nummod, eh? 
	std::cout.setf(std::ios::fixed);
	std::cout.precision(8);
	
	osmium::Location loc_start;
	std::vector<osmium::Location> loc_end;
	std::vector<way_data> waiz;
	graph debrecen_graph;
	//TODO: loc_ness
	
	loc_start = cmd_Start;
	loc_end = cmd_End;
	
	//std::cout << "# Navigating: " << loc_start << " -> " << loc_end << std::endl;
	
	//========================================================================================//
	//Parse shit
	osmium::io::Reader reader(cmd_MapPath.c_str());
	osmium::memory::Buffer buff;
	
	while(auto buff = reader.read())
	{
		for(auto& item: buff)
		{
			if(item.type() == osmium::item_type::node)
			{
				osmium::Node& n = static_cast<osmium::Node&>(item);
				graph::node gn;
				gn.data = node_data(n);
				
				debrecen_graph.data.insert({n.id(), gn});
				
				continue;
			}
			
			if(item.type() == osmium::item_type::way)
			  
			{
				osmium::Way& w = static_cast<osmium::Way&>(item);
				
				const char* highway = w.tags() ["highway"];
				if(!highway)
				  continue;
				
				if ( !strcmp ( highway, "footway" )
				      || !strcmp ( highway, "cycleway" )
				      || !strcmp ( highway, "bridleway" )
				      || !strcmp ( highway, "steps" )
				      || !strcmp ( highway, "path" )
				      || !strcmp ( highway, "construction" ) )
				  continue;

				way_data nw;
				for(auto& n : w.nodes())
					nw.push_back(n.positive_ref());
					
				waiz.push_back(nw);
			}
		}
	}
	reader.close();
	
	//========================================================================================//
	//Finding the closest node to the given points!
	
	osmium::object_id_type start_id;
	std::vector<osmium::object_id_type> end_id;
	end_id.resize(loc_end.size());
	
	if(cmd_StartId == 0 || cmd_EndId == 0)
	{
		start_id = debrecen_graph.data.begin()->first;
		for(auto& i: end_id)
			i = debrecen_graph.data.begin()->first;
		
		//Maybe some optim? [don't kill me]
		for(auto& it : debrecen_graph.data)
		{
			graph::id_type id = it.first;
			
			//Didn't like the long lines with namespaces, so replaced them with slightly less long 
			//(who am I kidding ) lines full of debrecen_graph calls
			double dist_start_stored = debrecen_graph.distance(loc_start, debrecen_graph.loc(start_id));
			double dist_start_current = debrecen_graph.distance(loc_start, debrecen_graph.loc(id));
			
			if (dist_start_current < dist_start_stored){
				start_id = id;
			}
			
			for(unsigned i=0; i<loc_end.size(); i++)
			{
				double dist_end_stored = debrecen_graph.distance(loc_end[i], debrecen_graph.loc(end_id[i]));
				double dist_end_current = debrecen_graph.distance(loc_end[i], debrecen_graph.loc(id));
				
				if (dist_end_current < dist_end_stored){
					end_id[i] = id;
				}
			}
		}
	}
	else
	{
		start_id = cmd_StartId;
		end_id.resize(1);
		end_id[0] = cmd_EndId;
		
		loc_start = debrecen_graph.loc(start_id);
		loc_end[0] = debrecen_graph.loc(end_id[0]);
	}
	
	std::cout << "# Translated to nodes: " << start_id << " -> ";
	for(auto& i: end_id)
		std::cout << i << " ";
	std::cout << std::endl;
	
	//========================================================================================//
	//Graph that crap!
	
	//Link nodez, #2chainz
	for(auto& w : waiz)
	{
		for(unsigned i=1; i<w.size(); i++) //Size actually mattress
			debrecen_graph.bilink(w[i],w[i-1]);
	}
	
	//std::cout << "# Straight distance: " << (debrecen_graph.distance(loc_start, loc_end)/1000.0) << " / " << (debrecen_graph.distance(start_id, end_id)/1000.0) << " (km's)" << std::endl;
	
	std::list<graph::id_type> route;
	
	if(cmd_UseDijkstra)
		route = debrecen_graph.multi_dijkstra(start_id, end_id);
	else
		std::cerr << "Keep your shirt on, sparky" << std::endl;
		//route = debrecen_graph.breadth_first_nav(start_id, end_id);
	
	std::cout << "# Route length: " << route.size() << " steps" << std::endl;
	
	std::cout << loc_start.lat() << " " << loc_start.lon() << std::endl;
	for(auto& i: route)
		std::cout << debrecen_graph.loc(i).lat() << " " << debrecen_graph.loc(i).lon() << std::endl;
	//std::cout << loc_end.lat() << " " << loc_end.lon() << std::endl;
	
	return 0;
}
