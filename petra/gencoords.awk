#/usr/bin/awk -f

BEGIN {
	min_x = 47.551705 + 0*47.573904;
	min_y = 21.599053 + 0*21.583532;
	
	max_x = 47.520355 + 0*47.507735;
	max_y = 21.640509 + 0*21.671423;
}

{ srand($0); }

END { 
	print min_x+rand()*(max_x-min_x) " " min_y+rand()*(max_y-min_y);
}
