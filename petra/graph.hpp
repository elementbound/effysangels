#ifndef _H_PETRA_GRAPH_H_
#define _H_PETRA_GRAPH_H_

#include <list>
#include <set>
#include <map>
#include <vector>

#include <osmium/osm.hpp>
#include <osmium/geom/haversine.hpp>

struct node_data
{
	osmium::object_id_type id;
	std::string name;
	osmium::Location loc;
	
	node_data() = default;
	node_data(const node_data&) = default;
	~node_data() = default;
	
	node_data(const osmium::Node& n);
};

class graph
{
	public:
		typedef osmium::object_id_type id_type;
		
		struct node 
		{
			node_data data;
			std::set<id_type> neighbors;
			
			node() = default;
			node(const node&) = default;
			~node() = default;
			node(node&&) = default;
			node& operator=(const node&)=default;
			node& operator=(node&&)=default;
			
			node(const node_data& d):
				data(d)
				{};
		};
		
		std::map<id_type, node> data;
		
		//
		
		osmium::Location loc(id_type n);
		std::set<id_type>& neighbors(id_type n);
		
		double distance(osmium::Location a, osmium::Location b);
		double distance(id_type a, id_type b);
		
		void bilink(id_type a, id_type b);
		
		std::list<id_type> breadth_first_nav(id_type a, id_type b);
		std::list<id_type> dijkstra(id_type a, id_type b);
		std::list<id_type> multi_dijkstra(id_type a, const std::vector<id_type>& b);
};

#endif
