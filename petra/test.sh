#!/bin/bash

do_route_builtin=0;
do_viewer_builtin=0;
do_viewer_random=0;
do_multiroute=0;
randomizer_count=0;

viewer_flags="";
router_flags="";

run_router="../bin/petra --map ../debrecen.osm $router_flags";
run_viewer="java Mapviewer $viewer_flags";

random_loc ()
{
	date +%N | awk -f ./gencoords.awk
}

view_file ()
{
	echo -e "\033[1m$1: \033[0m";
	cat $1 | awk '/^#/{ print $0;}';
	
	echo;

	$run_viewer $1 $viewer_flags > /dev/null 2> /dev/null;
}

for flag in $@; do
	if [ $flag == "--route-builtin" ]; then do_route_builtin=1; fi;
	if [ $flag == "--view-builtin" ]; then do_viewer_builtin=1; fi;
	if [ $flag == "--view-random" ]; then do_viewer_random=1; fi;
	if [ $flag == "--multiroute" ]; then do_multiroute=1; fi;
	if [[ $flag == --random-count=* ]]; then randomizer_count=${flag/*=/}; fi;
done;

loc_home="47.541163 21.649993";
loc_kossuth="47.555092 21.623687";
loc_ik="47.542432 21.639792";
loc_trap1="47.528230 21.609817";
loc_trap2="47.522985 21.603501";
loc_station="47.520869 21.627302";
loc_tesco="47.530948 21.610674";
loc_dioszeg="47.514571 21.658691";
loc_agrar="47.549508 21.604907";

if [ $do_multiroute == 1 ];
then
	echo "Multirouting...";
	$run_router --start $loc_home --end $loc_kossuth --end $loc_ik --end $loc_trap1 --end $loc_trap2 --end $loc_tesco > multi.route;
	view_file multi.route;
fi;

if [ $do_route_builtin == 1 ];
then
	echo "Naving from home to Kossuth"; 
	$run_router --start $loc_home --end $loc_kossuth > homesuth.route;
	
	echo "Naving from Kossuth to IK";
	$run_router --start $loc_kossuth --end $loc_ik > iksuth.route;
	
	echo "Naving from trap to trap";
	$run_router --start $loc_trap1 --end $loc_trap2 > trap.route;
	
	echo "Naving from station to TESCO";
	$run_router --start $loc_station --end $loc_tesco > stasco.route;
	
	echo "Naving from Diószeg str. to Agrár";
	$run_router --start $loc_dioszeg --end $loc_agrar > dioszgrar.route;
fi;

if (( randomizer_count > 0 ));
then
	echo "Doing random routes";
	
	while (( randomizer_count > 0 )); do
		echo "Doing test#$randomizer_count";
		$run_router --start $(random_loc) --end $(random_loc) > random_$randomizer_count.route;
		echo; 
		
		((randomizer_count--));
	done;
fi;

if [ $do_viewer_random == 1 ]; then
	for f in random_*.route; do
		view_file $f;
	done;
fi;

if [ $do_viewer_builtin == 1 ]; 
then
	for f in *.route; do
		if [[ $f == random_* ]]; then continue; fi;
		view_file $f;
	done;
fi;
