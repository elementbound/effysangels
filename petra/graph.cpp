#include "graph.hpp"

#include <iostream>
#include <queue>

node_data::node_data(const osmium::Node& n)
{
	id = n.id();
	name = n.tags().get_value_by_key("name","???");
	loc = n.location();
}

osmium::Location graph::loc(id_type n)
{
	return data[n].data.loc;
}

//adott idjű csomópont szomszédhalmazára ad egy referenciát
std::set<graph::id_type>& graph::neighbors(graph::id_type n)
{
	return data[n].neighbors;
}

//a haversine távolságot kiszámolja két koordináta között
double graph::distance(osmium::Location a, osmium::Location b)
{
	return osmium::geom::haversine::distance(a, b);
}

//mindkét id-hez lekéri a helyzetet és arra számol távolságot
//itt nem kell kiírnia h lekéred az id-hez tartozó csomópontot hanem egyszerűen megcsinálja
double graph::distance(graph::id_type a, graph::id_type b)
{
	return this->distance(data[a].data.loc, data[b].data.loc);
}

//összeköti a két csomópontot kétirányúan
void graph::bilink(graph::id_type a, graph::id_type b)
{
	data[a].neighbors.insert(b);
	data[b].neighbors.insert(a);
}

//szélességi bejáráson alapuló navigálás
std::list<graph::id_type> graph::breadth_first_nav(graph::id_type a, graph::id_type b)
{
	//Traversal
	//amiket meglátogattunk számlálóka
	unsigned visited = 0;
	
	//distancemap: minden csomópontra megmondja h ahhoz hogy eljussak oda
	//mennyit kell sétálnom a kezdőpontból
	std::map<id_type, double> dstmap;
	//kezdőpontból 0-t kell sétálni a kezdőpontba
	dstmap.insert({a,0});
	
	std::queue<std::pair<id_type,id_type>> to_visit;
	for(auto& n: this->neighbors(a))
		to_visit.push({a,n});
	
	while(!to_visit.empty())
	{
		//lekérdezem az adatot a sorból és popolom
		std::pair<id_type, id_type> pc = to_visit.front();
		to_visit.pop();
		
		id_type parent = pc.first;
		id_type current = pc.second;
		visited++;
		
		double dst = dstmap[parent] + this->distance(parent, current);
		
		// már meglátogattuk-e a csomópontot
		//megszámolja h a mapben az adott kulcsra hány találat van
		//egy kulcsra vagy 0 vagy 1 találat van
		if(dstmap.count(current))
		{
			if(dst < dstmap[current])
				dstmap[current] = dst;
			else
				continue;
		}
		else
		{
			//gyereknek a távolsága
			dstmap.insert({current, dst});
		}
		
		if(this->neighbors(current).size() == 0)
		{
			std::cerr << "Traversing error: found isolated node: " << current << "@" << this->loc(current) << std::endl;
			std::cout << "#[Error] Found isolated node: " << current << "@" << this->loc(current) << std::endl;
		}
		
		//gyerek összes szomszédját elpakoljuk hogy őket még meg kell néznünk
		for(auto& n: this->neighbors(current))
			to_visit.push({current,n});
			
		if((visited%2048) == 0)
			std::cerr << "Traversing: " << visited << ", " << to_visit.size() << " to go        \r";
	}
	std::cerr << std::endl;
	
	//Backtrack
	std::list<id_type> route;
	id_type at = b;
	id_type prev = a;   //előző ciklusban használt id
	
	while(1)
	{
		//följegyzem h éppen hol vagyok mindig a lista elejére
		route.push_front(at);
		prev = at;
		
		std::cerr << "Backtracing: " << b << " -> " << a << ": " << at << " (" << dstmap[at] << ")[" << route.size() << "]                \r";
		
		//ha a kezdőpontban vagyunk megállunk
		if(at == a) break;
		
		//ha ugyanabban a nodeban maradtunk, akkor végtelen ciklusba futnánk
		//( magányos kis izolált pontok debrecenben ftw )
		//if( prev == at ) break;
		
		id_type closest = at;
		double closest_dst = dstmap[at];
		//végigmegyünk a szomszédjain h melyik van legközelebb az elejéhez
		//melyiknek kisebb az értéke a dstmapben
		for(auto& n: this->neighbors(at))
			if(dstmap[n] < closest_dst)
			{
				closest_dst = dstmap[n];
				closest = n;
			}
			
		//és akkor odalép
		if(closest == at)
		{
			std::cerr << "Wound up on best but not destination node: " << at << "@" << this->loc(at) << std::endl;
			std::cout << "#[Error] Wound up on best but not destination node: " << at << "@" << this->loc(at) << std::endl;
			break;
		}
		
		at = closest;
	}
	
	std::cerr << std::endl;
	std::cerr << "Route length: " << dstmap[b] << std::endl;
	
	return route;
}

//--------------------------------------------------------------------------------------------//

std::list<graph::id_type> graph::dijkstra(graph::id_type a, graph::id_type b){	
	std::map<id_type,double> D;
	std::map<id_type,id_type> Previous;
	std::set<std::pair<double, id_type>> Q;
	
	for(auto& m: this->data){
		if(m.first == a){
				D.insert({a,0});
				Q.insert({0,a});
				Previous.insert({a, 0});
			}
		else{
			D.insert({m.first,INFINITY});
			Previous.insert({m.first,0});
			}
	}	
	
	double dst = 0;
	
	while(!Q.empty()){
		std::pair<double,id_type> top = *Q.begin();
        Q.erase(Q.begin());
        
        id_type u = top.second;
        
		for(auto& n: this->neighbors(u)){
				dst = D[u] + this->distance(u,n);
				if(dst < D[n]){
					if(D[n] != INFINITY)
						Q.erase({D[n], n});
					
					D[n] = dst;
					Previous[n] = u;
					
					Q.insert({dst, n});
				}
			}
	}
	
	std::list<id_type> Route;
	id_type u = b;
	while(Previous[u] != 0){
		Route.push_front(u);
		u = Previous[u];
	}
	
	std::cout << "# Route length: " << D[b] << " m" << std::endl;
	
	return Route;
}

std::list<graph::id_type> graph::multi_dijkstra(graph::id_type a, const std::vector<graph::id_type>& b){
	std::map<id_type,double> D;
	std::map<id_type,id_type> Previous;
	std::set<std::pair<double, id_type>> Q;
	
	for(auto& m: this->data){
		if(m.first == a){
				D.insert({a,0});
				Q.insert({0,a});
				Previous.insert({a, 0});
			}
		else{
			D.insert({m.first,INFINITY});
			Previous.insert({m.first,0});
			}
	}	
	
	double dst = 0;
	
	while(!Q.empty()){
		std::pair<double,id_type> top = *Q.begin();
        Q.erase(Q.begin());
        
        id_type u = top.second;
        
		for(auto& n: this->neighbors(u)){
				dst = D[u] + this->distance(u,n);
				if(dst < D[n]){
					if(D[n] != INFINITY)
						Q.erase({D[n], n});
					
					D[n] = dst;
					Previous[n] = u;
					
					Q.insert({dst, n});
				}
			}
	}
	
	id_type real_b = b[0];
	
	for(auto& bc: b){
		if(Previous[bc] == 0){
			continue;
		}
		
		std::cerr << "Checking node " << bc << ", dst: " << D[bc] << std::endl;
		
		if(D[bc] < D[real_b]){
			real_b = bc;
			std::cerr << "New closest: " << D[real_b] << std::endl;
		}
	}
	
	std::cerr << "Routing to node " << real_b << std::endl;
	std::list<id_type> Route;
	id_type u = real_b;
	while(Previous[u] != 0){
		Route.push_front(u);
		u = Previous[u];
	}
	
	std::cout << "# Route length: " << D[real_b] << " m" << std::endl;
	
	return Route;
	
}	
