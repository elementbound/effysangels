import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.DefaultWaypointRenderer;
import org.jxmapviewer.viewer.Waypoint;

public class ToWaypointRenderer extends DefaultWaypointRenderer
{    
    private BufferedImage img = null;

    /**
     * Uses a default waypoint image
     */
    public ToWaypointRenderer()
    {
        URL resource = getClass().getResource("images/to_waypoint.png");

        try
        {
            img = ImageIO.read(resource);
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Couldn't read to_waypoint.png.", "Warning!", JOptionPane.WARNING_MESSAGE);
            //System.out.println("Couldn't read to_waypoint.png.");
            //System.out.println(ex.getMessage());
            //ex.printStackTrace();
        }
    }
    
    @Override
    public void paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint w)
    {
        if(img != null){
            Point2D point = map.getTileFactory().geoToPixel(w.getPosition(), map.getZoom());
            
            int x = (int)point.getX();
            int y = (int)point.getY();
            
            g.drawImage(img, x - img.getWidth() / 2, y - img.getHeight(),null);
        }
        else{
            Point2D point = map.getTileFactory().geoToPixel(w.getPosition(), map.getZoom());
            
            int x = (int)point.getX();
            int y = (int)point.getY();
            
            g.setStroke(new BasicStroke(3f));
            g.setColor(Color.BLUE);
            g.drawOval(x-5,y-5,10,10);    
        }
    }
}
