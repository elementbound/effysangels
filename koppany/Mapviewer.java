import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.*;
import java.util.Scanner;
import java.lang.String;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.VirtualEarthTileFactoryInfo;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.DefaultWaypoint;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactoryInfo;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;
import org.jxmapviewer.viewer.TileFactory;
import org.jxmapviewer.painter.CompoundPainter;
import org.jxmapviewer.painter.Painter;
import org.jxmapviewer.input.CenterMapListener;
import org.jxmapviewer.input.PanKeyListener;
import org.jxmapviewer.input.ZoomMouseWheelListenerCursor;
import org.jxmapviewer.input.PanMouseInputListener;

/**
 * A simple sample application that shows
 * a OSM map of Europe containing a route with waypoints
 * @author Martin Steiger azt az nalmas kurvahétszencségit ennek az órának hogy agyhalott zabi leszek tőle
 vagy egy paranoid android mint marvin
 * */
public class Mapviewer
{
    /**
     * @param args the program args (ignored)
     */
    public static <T> void main(String[] args)
    {
        try{
            if (args.length != 2){
                System.out.println("I can't do anything. Please give me an input file and a bus number ( [22] ).");
                System.exit(1);
            }
            if ((args[1].indexOf("[") != 0) || (args[1].indexOf("]") != args[1].length()-1)){
                System.out.println("Wrong bus number format, try like this: [22].");
                System.exit(1);
            }

            // Create a TileFactory vector for Maps
            final List<TileFactory> factories = new ArrayList<TileFactory>();
            
            TileFactoryInfo osmInfo = new OSMTileFactoryInfo();
            TileFactoryInfo veInfo = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.HYBRID);
            
            factories.add(new DefaultTileFactory(osmInfo));
            factories.add(new DefaultTileFactory(veInfo));
            
            // Setup JxMapViewer
            final JXMapViewer mapViewer = new JXMapViewer();
            mapViewer.setTileFactory(factories.get(0));
            
            // Add interactions
            MouseInputListener mia = new PanMouseInputListener(mapViewer);
            mapViewer.addMouseListener(mia);    //ugrál nélküle
            mapViewer.addMouseMotionListener(mia);  //mozgatás
            mapViewer.addMouseWheelListener(new ZoomMouseWheelListenerCursor(mapViewer));   //zoom
            
            // Make a panel for switch maps
            JPanel panel = new JPanel();
            JLabel label = new JLabel("Select a TileFactory ");
               
            String[] tfLabels = new String[factories.size()];
            for (int i = 0; i < factories.size(); i++)
            {
                tfLabels[i] = factories.get(i).getInfo().getName();
            }
             
            final JComboBox<String> combo = new JComboBox<String>(tfLabels);
            combo.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    TileFactory factory = factories.get(combo.getSelectedIndex());
                    mapViewer.setTileFactory(factory);
                }
            });
            
            panel.setLayout(new GridLayout());
            panel.add(label);
            panel.add(combo);
            
        // Open route and make stops
            File file = new File(args[0]);
            FileReader fr = new FileReader(file) ;
            BufferedReader br = new BufferedReader(fr);
            
            String line;
            ArrayList<Double> lat1 = new ArrayList<Double>();
            ArrayList<Double> lon1 = new ArrayList<Double>();
            ArrayList<Double> lat2 = new ArrayList<Double>();
            ArrayList<Double> lon2 = new ArrayList<Double>();
            
            boolean inside = false;
            String bus = args[1];
            int counter = 0;
            
            while ((line = br.readLine()) != null) {
                if (line.contains(bus)){
                    inside = true;
                    counter++;
                    continue;
                }
                else if (line.indexOf('[') == 0 && inside){
                    inside = false;
                    continue;
                }
                if (inside && counter == 1){
                    String [] st = line.split(" ");
                    try{
                        if (st.length>0){
                            lon1.add(Double.parseDouble(st[0]));
                            lat1.add(Double.parseDouble(st[1]));
                        }
                    }
                    catch(Exception e){}
                }
                if (inside && counter == 2){
                    String [] st = line.split(" ");
                    try{
                        if (st.length>0){
                            lon2.add(Double.parseDouble(st[0]));
                            lat2.add(Double.parseDouble(st[1]));
                        }
                    }
                    catch(Exception e){}
                }
            }
            
            if (counter == 0){
                throw new Exception();
            }
            
            //Make two list for the two (or one) way of bus
            ArrayList<GeoPosition> stops1 = new ArrayList<GeoPosition>();
            ArrayList<GeoPosition> stops2 = new ArrayList<GeoPosition>();
            for (int i = 0; i < lat1.size(); i++) {
                stops1.add(new GeoPosition(lat1.get(i),lon1.get(i)));
            }
            for (int i = 0; i < lat2.size(); i++) {
                stops2.add(new GeoPosition(lat2.get(i),lon2.get(i)));
            }

            // Create waypoints from the geo-positions
            Set<Waypoint> waypoints1 = new HashSet<Waypoint>();
            for (GeoPosition a : stops1)
                waypoints1.add(new DefaultWaypoint(a));
            
            Set<Waypoint> waypoints2 = new HashSet<Waypoint>();
            for (GeoPosition a : stops2)
                waypoints2.add(new DefaultWaypoint(a));

            // Create two waypoint painter that takes all the waypoints
            WaypointPainter<Waypoint> waypointPainter1 = new WaypointPainter<Waypoint>();
            waypointPainter1.setWaypoints(waypoints1);
            waypointPainter1.setRenderer(new ToWaypointRenderer());
            
            WaypointPainter<Waypoint> waypointPainter2 = new WaypointPainter<Waypoint>();
            waypointPainter2.setWaypoints(waypoints2);
            waypointPainter2.setRenderer(new FromWaypointRenderer());
            
            // Create a compound painter that uses both the waypoint-painter
            List<Painter<JXMapViewer>> painters = new ArrayList<Painter<JXMapViewer>>();
            
            painters.add(waypointPainter1);
            painters.add(waypointPainter2);
            
            CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
            
            mapViewer.setOverlayPainter(painter);
            
            // Display the viewer in a JFrame
            JFrame frame = new JFrame("Mapviewer");
            frame.getContentPane().add(mapViewer);
            frame.add(panel, BorderLayout.NORTH);
            frame.setSize(800, 600);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
            frame.setLayout(new BorderLayout());
            
            // Set the focus
            HashSet<GeoPosition> first = new HashSet<GeoPosition>(stops1);
            mapViewer.zoomToBestFit(first, 0.7);
        }
        catch(FileNotFoundException e){
            //System.out.println("File Not Found!");
            JOptionPane.showMessageDialog(null, "File Not Found!", "Error!", JOptionPane.ERROR_MESSAGE); 
            System.exit(1);
        }
        catch(IOException e){
            //System.out.println("File can't be open!");
            JOptionPane.showMessageDialog(null, "File can't be open!", "Error!", JOptionPane.ERROR_MESSAGE); 
            System.exit(2);
        }
        catch(Exception e){
            //System.out.println("File can't be open!");
            JOptionPane.showMessageDialog(null, "Not existing bus route!", "Error!", JOptionPane.ERROR_MESSAGE); 
            System.exit(3);
        }
    }
}
