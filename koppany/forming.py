#!/usr/bin/env python
# encoding: utf8


import sys

def main():
    f = open(sys.argv[1],"r")
    to = open(sys.argv[2],"w")
    for line in f:
        if line[0] == "[":
            to.write(line[0:line.find("]")+1])
            to.write("\n")
        if line[0:4] == "    ":
            to.write(line[line.rfind("(")+1:line.rfind(")")].replace(","," "))
            to.write("\n")
    
    f.close()
    to.close()
########################

if __name__ == "__main__":
    main()
