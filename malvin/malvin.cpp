#include <iostream>
#include <osmium/osm.hpp>
#include <osmium/io/any_input.hpp>
#include <string>
#include <vector>
#include <map>

using namespace std;

//buszjáratok tárolása
struct bus_route
{
	std::string name;
	std::string from;
	std::string to;
	
	//a buszjárathoz tartozó megállók id-je
	std::vector<osmium::object_id_type> stops;
};

//buszmegállók tárolása
struct bus_stop
{
	std::string name;
	osmium::Location loc;
	
	bus_stop() = default;
	bus_stop(const bus_stop&) = default;
	~bus_stop() = default;
	
	bus_stop(const osmium::Node& n)
	{
		name = n.tags().get_value_by_key("name","???");
		loc = n.location();
	}
};

//<< operátor túlterhelése, kényelmes kiíratás érdekében
std::ostream& operator<<(std::ostream& os, const bus_stop& b)
{
	os << b.name << " " << b.loc;
	return os;
}

int main(int argc, char** argv)
{
	if(argc<2)
	{
		cout << "Missing parameter! Which map should I use?" << endl;
		return 1;
	}
	
	//az összes buszjárat egy vektorban lesz tárolva
	std::vector<bus_route> routes;
	//id szerint tárolva a buszmegállók adatai
	std::map<osmium::object_id_type, bus_stop> node_data;
	
	osmium::io::Reader reader(argv[1], osmium::osm_entity_bits::all);
	
	osmium::memory::Buffer buff;
	
	while(buff = reader.read())
	{
		for(auto& item: buff)
		{
			//ha node a beolvasott elem
			if(item.type() == osmium::item_type::node)
			{
				osmium::Node& n = static_cast<osmium::Node&>(item);
				if(n.tags().get_value_by_key("name"))
					//lehetséges buszmegállók letárolása
					node_data.insert({n.id(), bus_stop(n)});
					
				continue;
			}
			
			if(item.type() != osmium::item_type::relation)
				continue;
				
			osmium::Relation& r = static_cast<osmium::Relation&>(item);
			
			if(std::string(r.tags().get_value_by_key("route","")) != "bus")
				continue;
			
			//new_route-ba kiválogatjuk a buszjárat szükséges adatait és
			//a végén belepusholjuk a routes vektorba
			bus_route new_route;
			if(r.tags()["ref"])
				new_route.name = r.tags()["ref"];
			else if(r.tags()["name"])
				new_route.name = r.tags()["name"];
			else
				new_route.name = "???";
				
			new_route.from = r.tags()["from"];
			new_route.to = r.tags()["to"];
			
			//buszmegállók id-jának letárolása
			for(auto& m : r.members())
			{
				if(m.type() != osmium::item_type::node ||
					strcmp(m.role(), "stop") != 0)
					continue;
				
				new_route.stops.push_back(m.ref());
			}
			
			routes.push_back(new_route);
		}
	}
	
	//kiíratás
	for(auto& route : routes)
	{
		cout << "[" << route.name << "] " << route.from << " -> " << route.to << endl;
		for(auto& stop : route.stops)
			cout << "    " << node_data[stop] << endl;
		
		cout << endl;
	}
	
	cout << routes.size() << " bus route" << ((routes.size()>1)?"s":"") << "found." << endl;
	
	return 0;
}
