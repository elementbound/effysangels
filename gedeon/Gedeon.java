import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import br.zuq.osm.parser.*;
import br.zuq.osm.parser.model.*;
import br.zuq.osm.parser.util.*;

public class Gedeon {

    public static void main(String args[]) {
        OSMParser osmp = new OSMParser();
        String path = args[0];
        int db = 0;
        try {
            OSM map = osmp.parse(path);

            for (Relation rel: map.getRelations()) {
                List < String > bus_stops = new ArrayList < String > ();
                String bus_name = new String();
                String bus_to = new String();
                String bus_from = new String();
                boolean is_it_a_bus = false;

                for (Member memb: rel.members) {
                    if (memb.role.equals("stop")) {
                        for (Map.Entry < String, String > entry: rel.tags.entrySet()) {
                            if (entry.getKey().equals("route") && entry.getValue().equals("bus")) {
                                is_it_a_bus = true;
                            }
                        }
                        for (Map.Entry < String, String > entry: rel.tags.entrySet()) {
                            if (entry.getKey().equals("ref")) {
                                bus_name = entry.getValue();
                            } else if (entry.getKey().equals("from")) {
                                bus_from = entry.getValue();
                            } else if (entry.getKey().equals("to")) {
                                bus_to = entry.getValue();
                            }
                        }
                        for (OSMNode nodes: map.getNodes()) {
                            if (nodes.id.equals(memb.ref)) {
                                for (Map.Entry < String, String > entry: nodes.tags.entrySet()) {
                                    if (entry.getKey().equals("name")) {
                                        bus_stops.add(entry.getValue());
                                    }
                                }
                            }
                        }
                    }
                }
                if (!bus_stops.isEmpty() && is_it_a_bus == true) {
                    db++;
                    System.out.println("Járat:" + bus_name + " from: " + bus_from + " to: " + bus_to + " ");
                    for (String s: bus_stops) {
                        System.out.println(s);
                    }
                    System.out.println("=========================================================");
                }

            }
            System.out.println(db);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
